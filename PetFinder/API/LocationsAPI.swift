//
//  LocationsAPI.swift
//  PetFinder
//
//  Created by Dunja Maksimovic on 12/25/19.
//  Copyright © 2019 dunjamaksimovic. All rights reserved.
//

import Foundation

class LocationsAPI: BaseAPI {
    
    static func getLocations() {
        
        BaseAPI.executeRequest(path: "", method: "get") { (result) in
            
            switch result {
            case .success(let data):
                print(data)
            case .failure(let error):
                print(error)
            }
        }
    }
}

