//
//  CustomPin.swift
//  PetFinder
//
//  Created by Dunja Maksimovic on 2/4/20.
//  Copyright © 2020 dunjamaksimovic. All rights reserved.
//

import UIKit
import MapKit

class CustomPin: NSObject, MKAnnotation {
	
	var coordinate: CLLocationCoordinate2D
	
	var title: String? = " "
	var location: Datum?
	
	init(coordinate: CLLocationCoordinate2D, location: Datum?) {
		self.coordinate = coordinate
		self.location = location
	}
}
