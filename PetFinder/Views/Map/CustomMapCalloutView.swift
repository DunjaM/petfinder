//
//  CustomMapCalloutView.swift
//  PetFinder
//
//  Created by Dunja Maksimovic on 2/4/20.
//  Copyright © 2020 dunjamaksimovic. All rights reserved.
//

import UIKit

class CustomMapCalloutView: UIView {

	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var subtitleLabel: UILabel!
	
	var location: Datum?

	override func awakeFromNib() {
		
		configFontsAndColors()
	}
	
	private func configFontsAndColors() {
		
		// Fonts
		titleLabel.font = Fonts.title_medium
		subtitleLabel.font = Fonts.primary_small
		
		// Colors
		titleLabel.textColor = Colors.txt_primary
		subtitleLabel.textColor = Colors.txt_primary
	}
	
	func configWith(location: Datum?) {
		
		self.location = location
		
		titleLabel.text = location?.title
		subtitleLabel.text = location?.datumDescription
		
		if let image = location?.animal?.image?.path {
			Utils.setImageViaAlamofireImage(image, to: imageView)
		}
	}
}
