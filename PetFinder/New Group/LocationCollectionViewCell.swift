//
//  LocationCollectionViewCell.swift
//  PetFinder
//
//  Created by Dunja Maksimovic on 12/27/19.
//  Copyright © 2019 dunjamaksimovic. All rights reserved.
//

import UIKit

class LocationCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
