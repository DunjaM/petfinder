//
//  Fonts.swift
//  PetFinder
//
//  Created by Dunja Maksimovic on 2/4/20.
//  Copyright © 2020 dunjamaksimovic. All rights reserved.
//

import UIKit

struct Fonts {
	
	static let title_medium = UIFont(name: "Avenir-Bold", size: 14)
	static let title_large = UIFont(name: "Avenir-Bold", size: 18)
	
	static let primary_small = UIFont(name: "Avenir", size: 12)
	static let primary_medium = UIFont (name: "Avenir", size: 14)
	
	static let secondary_small = UIFont(name: "RobotoSlab-Regular", size: 12)
	static let secondary_medium = UIFont(name: "RobotoSlab-Regular", size: 14)
	static let secondary_large = UIFont(name: "RobotoSlab-Regular", size: 18)

	static let button_small = UIFont(name: "RobotoSlab-Bold", size: 12)
	static let button_medium = UIFont(name: "RobotoSlab-Bold", size: 14)
}
