//
//  LocationCollectionViewCell.swift
//  PetFinder
//
//  Created by Dunja Maksimovic on 1/6/20.
//  Copyright © 2020 dunjamaksimovic. All rights reserved.
//

import UIKit

class LocationCollectionViewCell: UICollectionViewCell {
	
	// MARK: - Outlets
	
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var subtitleLabel: UILabel!
	
	// MARK: - Properties
	
	var location: Datum?

	// MARK: - Lifecycle
	
	override func awakeFromNib() {
        super.awakeFromNib()
		
    }
	
	override func layoutSubviews() {
		configUI()
	}
	
	// MARK: - Functions
	
	private func configUI() {
		
		// Fonts
		titleLabel.font = Fonts.title_medium
		subtitleLabel.font = Fonts.primary_small
		
		// Colors
		titleLabel.textColor = Colors.txt_primary
		subtitleLabel.textColor = Colors.txt_primary
		
		// Corner radius
		containerView.addRoundedShadow()
		imageView.addRoundedShadow()
		imageView.clipsToBounds = true
	}
	
	func configWith(location: Datum) {
		
		self.location = location
		
		titleLabel.text = location.title
		subtitleLabel.text = location.datumDescription
		
		if let image = location.animal?.image?.path {
			Utils.setImageViaAlamofireImage(image, to: imageView)
		}
	}
}
