//
//  BaseAPI.swift
//  PetFinder
//
//  Created by Dunja Maksimovic on 12/25/19.
//  Copyright © 2019 dunjamaksimovic. All rights reserved.
//

import Foundation

class BaseAPI {
    
    static func executeRequest(path: String, method: String, completionHandler: @escaping (Result<Data, Error>)->()) {
        
        guard let url = URL(string: path) else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let data = data {
                completionHandler(.success(data))
            } else if let error = error {
                completionHandler(.failure(error))
            }
        }
        
        task.resume()
    }
}
