//
//  Extensions.swift
//  PetFinder
//
//  Created by Dunja Maksimovic on 1/6/20.
//  Copyright © 2020 dunjamaksimovic. All rights reserved.
//

import UIKit

extension UIView {
	
	static var identifier: String {
		return String(describing: self)
	}
	
	// Functions for awaking uiview from nib
	public class func fromNib(_ nibNameOrNil: String? = nil) -> Self {
		return fromNib(nibNameOrNil, type: self)
	}
	
	public class func fromNib<T : UIView>(_ nibNameOrNil: String? = nil, type: T.Type) -> T {
		let v: T? = fromNib(nibNameOrNil, type: T.self)
		return v!
	}
	
	public class func fromNib<T : UIView>(_ nibNameOrNil: String? = nil, type: T.Type) -> T? {
		var view: T?
		let name: String
		if let nibName = nibNameOrNil {
			name = nibName
		} else {
			// Most nibs are demangled by practice, if not, just declare string explicitly
			name = nibName
		}
		let nibViews = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
		for v in nibViews! {
			if let tog = v as? T {
				view = tog
			}
		}
		return view
	}
	
	public class var nibName: String {
		let name = "\(self)".components(separatedBy: ".").first ?? ""
		return name
	}
	public class var nib: UINib? {
		if let _ = Bundle.main.path(forResource: nibName, ofType: "nib") {
			return UINib(nibName: nibName, bundle: nil)
		} else {
			return nil
		}
	}
	
	func addRoundedShadow() {
		
		// Corner radius
		let cornerRadius = min(frame.width, frame.height) / 10
		
		
		layer.cornerRadius = cornerRadius
		clipsToBounds = true
		
        layer.shadowPath =
			  UIBezierPath(roundedRect: bounds,
			  cornerRadius: layer.cornerRadius).cgPath
		layer.shadowColor = UIColor.black.cgColor
		layer.shadowOpacity = 0.3
		layer.shadowOffset = CGSize(width: 0, height: 1)
		layer.shadowRadius = 1
		layer.masksToBounds = false
    }
}

extension UIApplication {
    
    // Get top view controller
    class func topViewController(_ base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController! {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}

extension UIColor {
	
	// Make UIColor from String
	convenience init(hexString: String) {
		
		let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
		var int = UInt32()
		Scanner(string: hex).scanHexInt32(&int)
		let a, r, g, b: UInt32
		switch hex.count {
		case 3: // RGB (12-bit)
			(a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
		case 6: // RGB (24-bit)
			(a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
		case 8: // ARGB (32-bit)
			(a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
		default:
			(a, r, g, b) = (255, 0, 0, 0)
		}
		self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
	}
}
