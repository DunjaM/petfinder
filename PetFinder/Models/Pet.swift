//
//  Pet.swift
//  PetFinder
//
//  Created by Dunja Maksimovic on 12/25/19.
//  Copyright © 2019 dunjamaksimovic. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let item = try? newJSONDecoder().decode(Item.self, from: jsonData)

import Foundation

// MARK: - Item

struct Item: Codable {
	
    let status: String?
    let state: String?
    let message: String?
    let data: [Datum]?
}

// MARK: - Datum

struct Datum: Codable, Equatable {
	
	static func == (lhs: Datum, rhs: Datum) -> Bool {
		return lhs.id == rhs.id
	}
	
    let id: String?
    let user: User?
    let title: String?
    let slug: String?
    let datumDescription: String?
    let latitude: Double?
    let longitude: Double?
    let created: String?
    let priority: String?
    let type: String?
    let address: String?
    let phone: String?
    let award: String?
    let animal: Animal?
    let images: [Image]?
    let files: [Image]?
	
	enum CodingKeys: String, CodingKey {
		
        case id
        case user
        case title
        case slug
        case datumDescription = "description"
        case latitude
        case longitude
        case created
        case priority
        case type
        case address
        case phone
        case award
        case animal
        case images
        case files
    }
}

// MARK: - Animal

struct Animal: Codable {
	
    let type: String?
    let color: String?
    let year: String?
    let name: String?
    let note: String?
    let idNumber: String?
    let breed: Breed?
    let image: Image?
}

// MARK: - Breed

struct Breed: Codable {
	
    let id: Int?
    let name: String?
}

// MARK: - Image

struct Image: Codable {
	
    let id: Int?
    let fileName: String?
    let path: String?
    let position: Int?
    let association: String?
}

// MARK: - User

struct User: Codable {
	
    let id: String?
    let firstName: String?
    let lastName: String?
    let email: String?
    let alias: String?
    let avatar: String?
}

