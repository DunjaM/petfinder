//
//  Utils.swift
//  PetFinder
//
//  Created by Dunja Maksimovic on 1/6/20.
//  Copyright © 2020 dunjamaksimovic. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import Photos
import SafariServices
import AlamofireImage

class Utils {
	
	// MARK:- Screen size
	
	/**
	Get screen width
	
	- returns: Screen width
	*/
	static func screenWidht() -> CGFloat {
		return UIScreen.main.bounds.width
	}
	
	/**
	Get screen height
	
	- returns: Screen height
	*/
	static func screenHeight() -> CGFloat {
		return UIScreen.main.bounds.height
	}
	
	static func topMargin() -> CGFloat {
		let window = UIApplication.shared.keyWindow
		if #available(iOS 11.0, *) {
			if let topMargin = window?.safeAreaInsets.top {
				return topMargin
			}
		} else {
			if let topGuide = window?.layoutMargins.top {
				return topGuide
			}
		}
		return 0.0
	}
	
	static func bottomMargin() -> CGFloat {
		let window = UIApplication.shared.keyWindow
		if #available(iOS 11.0, *) {
			if let bottomMargin = window?.safeAreaInsets.bottom {
				return bottomMargin
			}
		} else {
			if let bottomGuide = window?.layoutMargins.bottom {
				return bottomGuide
			}
		}
		return 0.0
	}
	
	// MARK:- Alert
	
	/**
	Show alert for 1 second
	*/
	static func showInfoAlert(message: String) {
		let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
		
		if UIApplication.topViewController() != alert {
			UIApplication.topViewController().present(alert, animated: true)
		}
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
			alert.dismiss(animated: true)
		}
	}
	
	static func showInfoAlert(message: String, completion: @escaping ()->()) {
		let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
		
		if UIApplication.topViewController() != alert {
			UIApplication.topViewController().present(alert, animated: true)
		}
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
			alert.dismiss(animated: true)
			completion()
		}
	}
	
	// MARK:- URL from string
	
	/**
	Get URL from string
	
	- returns: URL
	*/
	static func getUrlFromString(_ stringUrl: String?) -> URL {
		if let imageUrlString = stringUrl {
			if let url = URL(string: imageUrlString) {
				return url
			}
		}
		return URL(string: "")!
	}
	
	// MARK:- Open url
	
	/**
	Call phone number
	
	- parameter number: String number
	*/
	static func callNumber(_ number: String) {
		
		let num = number.replacingOccurrences(of: " ", with: "")
		
		if let numberUrl = URL(string: "tel://\(num)") {
			
			if UIApplication.shared.canOpenURL(numberUrl) {
				UIApplication.shared.open(numberUrl, options: [:], completionHandler: nil)
				
			} else {
				self.alertWithTitle("ERROR", message: NSLocalizedString("Not able to call number", comment: ""))
			}
		}
	}
	
	/**
	Open web page with url string
	
	- parameter urlString: String url
	*/
	static func openUrl(_ urlString: String) {
		
		if let theUrl = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? urlString) {
			
			let safariVC = SFSafariViewController(url: theUrl)
			
			if UIApplication.topViewController() != safariVC {
				UIApplication.topViewController().present(safariVC, animated: true, completion: nil)
			}
		}
	}
	
	/**
	Open location and create direction from current location to destination
	
	- parameter location: Destionation
	- parameter name:     Name of destination
	*/
	static func openLocationInMaps(_ location: CLLocationCoordinate2D, name: String?) {
		
		let placemark = MKPlacemark(coordinate: location, addressDictionary: nil)
		let mapItem = MKMapItem(placemark: placemark)
		
		mapItem.name = name ?? ""
		
		let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
		
		mapItem.openInMaps(launchOptions: launchOptions)
	}
	
	// MARK:- Alert
	
	/**
	Alert with title. One button is predefined ("OK")
	
	- parameter title:   Title message of alert
	- parameter message: Message of alert
	*/
	static func alertWithTitle(_ title: String, message: String) {
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
			switch action.style {
			case .default:
				print("default")
				_ = UIApplication.topViewController().popoverPresentationController
				
			case .cancel:
				print("cancel")
				
			case .destructive:
				print("destructive")
			
			@unknown default:
				print("unknown default case action style")
			}
		}))
		
		DispatchQueue.main.async(execute: {
			if UIApplication.topViewController() != alert {
				UIApplication.topViewController().present(alert, animated: true)
			}
		})
	}
	
	// MARK:- Alamofire Image Helper
	
	/**
	Set image to Image View
	
	* Only when using "Alamofire Image" library *
	*/
	static func setImageViaAlamofireImage(_ stringUrl: String?, to imageView: UIImageView) {
		if let imageUrlString = stringUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
			if let url = URL(string: imageUrlString) {
				DispatchQueue.main.async {
					imageView.af_setImage(withURL: url, placeholderImage: nil, imageTransition: .crossDissolve(0.3))
					return
				}
			}
		}
		imageView.image = UIImage(named: "placeholder")
	}
	
	// MARK: - Date formatter
	
	static func formatDateString(_ string: String) -> String {
		let date = dateFromString(string)
		let formatedDateString = stringFromDate(date)
		
		return formatedDateString
	}
	
	static func dateFromString(_ dateString: String) -> Date? {
		
		let dateString = dateString
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
		
		dateFormatter.timeZone = TimeZone.autoupdatingCurrent
		let date = dateFormatter.date(from: dateString)
		
		return date
	}

	static func stringFromDate(_ date: Date?) -> String {
		
		if date != nil {
			
			let dateFormatter = DateFormatter()
			dateFormatter.dateFormat = "dd.MM.yyyy"
			dateFormatter.timeZone = TimeZone.autoupdatingCurrent
			return dateFormatter.string(from: date!)
		} else {
			
			return ""
		}
	}
	
	static func milisecondsToMinutes(_ miliseconds: Int) -> String {
		
		let seconds: Double = Double(miliseconds / 1000)
		let (hr,  minf) = modf (seconds / 3600)
		let (min, secf) = modf (60 * minf)
		let sec = secf * 60
		
		let hours = Int(hr)
		let minutes = Int(min)
		let secs = Int(sec)
		
		if hours > 0 {
			return String(format: "%01d:%02d:%02d", hours, minutes, secs)
		} else {
			return String(format: "%02d:%02d", minutes, secs)
		}
	}
	
	func secondsToHoursMinutesSeconds (seconds : Double) -> (Double, Double, Double) {
		
		let (hr,  minf) = modf (seconds / 3600)
		let (min, secf) = modf (60 * minf)
		
		return (hr, min, 60 * secf)
	}
	
	// A delay function
	static func delay(seconds: Double, completion: @escaping ()-> Void) {
		DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: completion)
	}
}
