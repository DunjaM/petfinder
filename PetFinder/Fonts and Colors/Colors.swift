//
//  Colors.swift
//  PetFinder
//
//  Created by Dunja Maksimovic on 2/4/20.
//  Copyright © 2020 dunjamaksimovic. All rights reserved.
//

import UIKit

struct Colors {
	
	// Font colors
	static let txt_primary = UIColor(hexString:"#2b3c50")
	static let txt_grey = UIColor(hexString: "#455167")
	static let txt_light = UIColor(hexString: "#68758d")
	static let txt_white = UIColor(hexString: "#ffffff")
	static let txt_blue = UIColor(hexString: "#0b81b9")
	
	// App colors
	static let primary = UIColor(hexString: "#ffffff")
	static let primary_dark = UIColor(hexString: "#f1f5f7")
	static let accent = UIColor(hexString: "#0b81b9")
}
