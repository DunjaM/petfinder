//
//  ViewController.swift
//  PetFinder
//
//  Created by Dunja Maksimovic on 12/25/19.
//  Copyright © 2019 dunjamaksimovic. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {
	
	// MARK: - Outlets

	@IBOutlet weak var mapView: MKMapView!
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var myLocationButton: UIButton!
	
	// MARK: - Properties
	
	var locations = [Datum]()
	
	var locationManager = CLLocationManager()
	
	// MARK: - Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		getLocations()
		
		setupUI()
		setupCollectionView()
	}
	
	// MARK: - Setup
	
	private func setupUI() {
		
		myLocationButton.addRoundedShadow()
	}
	
	private func setupCollectionView() {
		
		collectionView.register(UINib(nibName: LocationCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: LocationCollectionViewCell.identifier)
		collectionView.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
	}

	// MARK: - Map view
	
	private func updateMapView(locations: [Datum]?) {
		
		guard let locations = locations else {
			return
		}
		
		// Locations
		self.locations = locations
		
		// Annotations
		addAnnotations(locations: locations)
		
		// Set margins for map
		mapView.layoutIfNeeded()
		mapView.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: collectionView.frame.height + 10, right: 10)
		
		// Show all pins with that margin
		mapView.showAnnotations(mapView.annotations, animated: true)
	}
	
	private func showAllLocations() {
		
		mapView.showsUserLocation = false
		
		
	}
	
	private func showUserLocation() {
		
		
	}
	
	private func addAnnotations(locations: [Datum]) {
		
		var annotations = [CustomPin]()
		
		for location in locations {
			
			if let lat = location.latitude, let lon = location.longitude {
				
				let annotation = CustomPin(coordinate: CLLocationCoordinate2DMake(lat, lon), location: location)
				annotations.append(annotation)
			}
		}
		
		mapView.addAnnotations(annotations)
	}
	
	// MARK: - Locations
	
	private func getLocations() {
		
		guard let url = Bundle.main.url(forResource: "locations", withExtension: "json") else {
			return
		}
		
		do {
			let data = try Data(contentsOf: url)
			
			do {
				let locations = try JSONDecoder().decode(Item.self, from: data)
				updateMapView(locations: locations.data)
				
			} catch {
				print(error)
			}
		} catch {
			print(error)
		}
	}
	
	// MARK: - Actions
	
	@IBAction func showMyLocation(_ sender: UIButton) {
		
		// Show user location
		if !mapView.showsUserLocation {
			startLocationManager()
		
		// Hide user location
		} else {
			mapView.showsUserLocation = false
		}
		
		// Zoom map to fit all annotations
		mapView.showAnnotations(mapView.annotations, animated: true)
		
		// Update button
		sender.isSelected = mapView.showsUserLocation
	}
}

extension MapViewController: MKMapViewDelegate {
	
	func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
		
		let identifier = "Annotation"
		
		var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
		
		if annotationView == nil {
			annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
			annotationView?.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
			annotationView?.canShowCallout = true
			
			// User location
			if annotation is MKUserLocation {
				annotationView?.image = UIImage(named: "userLocation")
				
			// Pet location
			} else {
				annotationView?.image = UIImage(named: "pin")
			}
			
		} else {
			annotationView?.annotation = annotation
		}
		
		// Callout view
		if let customPin = annotation as? CustomPin {
			
			let callout = CustomMapCalloutView.fromNib()
			callout.configWith(location: customPin.location)
			
			annotationView?.leftCalloutAccessoryView = nil
			annotationView?.rightCalloutAccessoryView = nil
			annotationView?.detailCalloutAccessoryView = callout
		}
		
		return annotationView
	}
	
	func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
		
		// Find selected location
		if let annotation = view.annotation as? CustomPin {
			
			if let selectedLocation = annotation.location {
				
				// Show location in scroll view
				if let index = locations.firstIndex(of: selectedLocation) {
					collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: true)
				}
			}
		}
	}
}

// MARK: - Collection View

extension MapViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		return locations.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LocationCollectionViewCell.identifier, for: indexPath) as! LocationCollectionViewCell
		
		let location = locations[indexPath.item]
		cell.configWith(location: location)
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		
		let width = collectionView.frame.width * 0.8
		
		return CGSize(width: width, height: width / 2)
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
		// Find selected location
		let selectedLocation = locations[indexPath.item]
			
		// Show location in map view
		if let annotation = mapView.annotations.first(where: { annotation -> Bool in
			if let customPin = annotation as? CustomPin {
				return customPin.location == selectedLocation
			}
			return false
		}) {
			mapView.selectAnnotation(annotation, animated: true)
		}
	}
}

// MARK: - Core Location

extension MapViewController: CLLocationManagerDelegate {
	
	func startLocationManager() {
		
		locationManager.requestWhenInUseAuthorization()
		
		requestUserLocation()
		
		mapView.showsUserLocation = true
	}
	
	func requestUserLocation() {
		
		if CLLocationManager.locationServicesEnabled() {
			locationManager.delegate = self
			locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
			locationManager.requestLocation()
		}
	}
	
	func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
		
		if status == .authorizedWhenInUse {
			requestUserLocation()
		}
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		
//		guard let locationValue = manager.location else {
//			return
//		}
	}
	
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		
		print("error requesting location")
		print(error.localizedDescription)
	}
}

